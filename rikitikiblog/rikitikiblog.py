"""
All the methods required for the blog to work.
"""

import os
import datetime
import json
from typing import Dict, List, Tuple
import pandoc

from config import load_config


_PATH_MARKDOWN_DIR = './markdown'

PostInfo = Tuple[str, str, Dict[str, str], str]

_CONFIG = load_config()


def _load_template_content(fname: str) -> str:
    with open(f'./templates/{fname}', 'r', encoding='utf-8') as fp:
        return fp.read()


def _load_theme(name: str) -> str:
    with open(f'./themes/{name}.json', 'r', encoding='utf-8') as fp:
        return json.load(fp)


def split_metadata_and_content(markdown: str) -> Tuple[Dict[str, str], str]:
    """
    Parse markdown file to extract the YAML header.  
    Parse the header to get key/val pairs of metadata.  
    Return metadata & content.

    :param markdown: content of the markdown file
    :return: metadata, content
    """
    if '---' not in markdown:
        raise ValueError("Invalid markdown file. Your markdown should always have a YAML header, as shown in the markdown/test-post.md file")
    _, header, content = markdown.split('---', maxsplit=2)

    lines = header.split('\n')
    metadata: Dict[str, str] = {}

    for line in lines:
        if line == "":
            continue
        key, val = line.split(':', maxsplit=1)
        metadata[key] = val.strip()
    if 'date' not in metadata:
        metadata['date'] = '1970-01-01'

    return metadata, content


def load_all_markdown_posts() -> List[PostInfo]:
    files = [f for f in os.listdir(_PATH_MARKDOWN_DIR) if not f.startswith('SPECIAL')]
    posts = []
    for f in files:
        with open(os.path.join(_PATH_MARKDOWN_DIR, f), 'r', encoding='utf-8') as fp:
            txt = fp.read()
        metadata, content = split_metadata_and_content(txt)
        is_draft = metadata.get("draft", False)
        if is_draft is False or is_draft == "False":
            posts.append((metadata['date'], f.replace('.md', ''), metadata, content))

    return posts


def generate_html_post(slug: str, metadata: Dict[str, str], content: str, template: str) -> str:
    content_ = pandoc.read(content, format='markdown')
    published_on = datetime.datetime.fromisoformat(metadata['date']).strftime('%d %b %Y')
    html = template.replace('{TITLE}', metadata['title'])
    html = html.replace('{AUTHOR_NAME}', _CONFIG['author-name'])
    html = html.replace('{PUBLISHED_ON}', published_on)
    html = html.replace('{POST}', pandoc.write(content_, format='html'))
    html = html.replace('{SLUG}', slug)
    html = html.replace('{BLOG_NAME}', _CONFIG['blog-name'])
    html = html.replace('{BLOG_DESCRIPTION}', _CONFIG['blog-description'])
    html = html.replace('{BLOG_URL}', _CONFIG['blog-url'])
    html = html.replace('{AUTHOR_EMAIL}', _CONFIG['author-email'])
    return html


def load_and_generate_all_posts() -> List[PostInfo]:
    post_template = _load_template_content('post_template.html')
    posts = load_all_markdown_posts()
    posts.sort()

    for _, slug, metadata, content in posts:
        html = generate_html_post(slug, metadata, content, post_template)
        with open(os.path.join('../www/', f"{slug}.html"), 'w', encoding='utf-8') as fp:
            fp.write(html)

    return posts


def generate_archive_page(posts: List[PostInfo]) -> None:
    archive_template = _load_template_content("archive_template.html")
    index_str = "<ul>\n"
    for _, slug, metadata, _ in posts[::-1]:
        published_on = datetime.datetime.fromisoformat(metadata["date"]).strftime('%d %b %Y')
        index_str += f"<li>{published_on}: <a href='./{slug}'>{metadata['title']}</a></li>\n"
    index_str += "</ul>\n"

    archive_template = archive_template.replace('{BLOG_NAME}', _CONFIG['blog-name'])
    archive_template = archive_template.replace('{BLOG_DESCRIPTION}', _CONFIG['blog-description'])
    archive_template = archive_template.replace('{BLOG_URL}', _CONFIG['blog-url'])
    archive_template = archive_template.replace('{AUTHOR_EMAIL}', _CONFIG['author-email'])
    archive_template = archive_template.replace('{AUTHOR_NAME}', _CONFIG['author-name'])

    with open('../www/archives.html', 'w', encoding='utf-8') as fp:
        fp.write(archive_template.replace('{INDEX}', index_str))


def generate_index_page(posts: List[PostInfo]) -> None:
    index_template = _load_template_content("index_template.html")
    _, slug, metadata, content = posts[-1]
    content_ = pandoc.read(content, format='markdown')
    published_on = datetime.datetime.fromisoformat(metadata["date"]).strftime('%d %b %Y')
    html = index_template.replace('{LAST_POST_TITLE}', metadata['title'])
    html = html.replace('{LAST_POST_AUTHOR}', _CONFIG['author-name'])
    html = html.replace('{LAST_POST_PUBLISHED_ON}', published_on)
    html = html.replace('{LAST_POST_CONTENT}', pandoc.write(content_, format='html'))
    html = html.replace('{SLUG}', slug)
    html = html.replace('{BLOG_NAME}', _CONFIG['blog-name'])
    html = html.replace('{BLOG_DESCRIPTION}', _CONFIG['blog-description'])
    html = html.replace('{BLOG_URL}', _CONFIG['blog-url'])
    html = html.replace('{AUTHOR_EMAIL}', _CONFIG['author-email'])
    html = html.replace('{AUTHOR_NAME}', _CONFIG['author-name'])

    index_str = "<ul>\n"
    for _, slug, metadata, _ in posts[::-1][1:6]:
        published_on = datetime.datetime.fromisoformat(metadata["date"]).strftime('%d %b %Y')
        index_str += f"<li>{published_on}: <a href='./{slug}.html'>{metadata['title']}</a></li>\n"
    index_str += "</ul>\n"
    html = html.replace('{LATEST_POSTS}', index_str)

    with open('../www/index.html', 'w', encoding='utf-8') as fp:
        fp.write(html)


def generate_feed(posts: List[PostInfo]) -> None:
    template_feed = _load_template_content('feed_template.xml')
    template_element = _load_template_content('feed_element_template.xml')

    elements_str = ""
    for _, slug, metadata, content in posts[::-1][:20]:
        content_ = pandoc.read(content, format='markdown')
        published_on = datetime.datetime.fromisoformat(metadata["date"]).strftime('%Y-%m-%d')
        el = template_element.replace("{TITLE}", metadata["title"])
        el = el.replace("{SLUG}", slug)
        el = el.replace("{TEXT}", pandoc.write(content_, format='html'))
        el = el.replace("{PUBLISHED_ON}", published_on)
        el = el.replace('{AUTHOR_EMAIL}', _CONFIG['author-email'])
        el = el.replace('{AUTHOR_NAME}', _CONFIG['author-name'])
        elements_str += el

    feed = template_feed.replace("{ELEMENTS}", elements_str)
    feed = feed.replace('{BLOG_NAME}', _CONFIG['blog-name'])
    feed = feed.replace('{BLOG_DESCRIPTION}', _CONFIG['blog-description'])
    feed = feed.replace('{BLOG_URL}', _CONFIG['blog-url'])
    feed = feed.replace('{AUTHOR_EMAIL}', _CONFIG['author-email'])
    feed = feed.replace('{AUTHOR_NAME}', _CONFIG['author-name'])
    with open('../www/feed.xml', 'w', encoding='utf-8') as fp:
        fp.write(feed)


def generate_about_page() -> None:
    template_about = _load_template_content('about_template.html')

    with open(os.path.join(_PATH_MARKDOWN_DIR, 'SPECIAL.about.md'), 'r', encoding='utf-8') as fp:
        about_content = fp.read()

    about = template_about.replace("{ABOUT}", about_content)
    about = about.replace('{BLOG_NAME}', _CONFIG['blog-name'])
    about = about.replace('{BLOG_DESCRIPTION}', _CONFIG['blog-description'])
    about = about.replace('{BLOG_URL}', _CONFIG['blog-url'])
    about = about.replace('{AUTHOR_EMAIL}', _CONFIG['author-email'])
    about = about.replace('{AUTHOR_NAME}', _CONFIG['author-name'])
    with open('../www/about.html', 'w', encoding='utf-8') as fp:
        fp.write(about)


def generate_css() -> None:
    template_css = _load_template_content('main_template.css')
    theme = _load_theme(_CONFIG["theme"])

    css = template_css.replace("{DARKEST_COLOR}", theme["DARKEST_COLOR"])
    css = css.replace("{MED_COLOR}", theme["MED_COLOR"])
    css = css.replace("{LIGHT_COLOR}", theme["LIGHT_COLOR"])
    css = css.replace("{LIGHTEST_COLOR}", theme["LIGHTEST_COLOR"])
    css = css.replace("{ADDITIONAL_CSS_RULES}", theme["ADDITIONAL_CSS_RULES"])

    with open('../www/main.css', 'w', encoding='utf-8') as fp:
        fp.write(css)



def publish():
    posts = load_and_generate_all_posts()
    print(f"{len(posts)} posts have been found in the markdown folder (excluding drafts)")
    generate_archive_page(posts)
    generate_index_page(posts)
    generate_feed(posts)
    generate_about_page()
    generate_css()
