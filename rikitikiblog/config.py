"""
CLI interface to setup the `var/config.json` file.

You can also play with the file directly if you like JSON.
"""
import json


CONFIGURATION_SETUP = [
    ("blog-name", "mon rikitikiblog", "Nom du blog", "Apparaitra comme titre."),
    ("blog-description", "moi et mes mots", "Description", "Ce sera écris sous le titre sur la page d'accueil"),
    ("author-name", "moi", "Nom de l'auteur", "La signature sous les posts"),
    ("author-email", "contact@moi.com", "Email de l'auteur", "Comment vous contacter"),
    ("blog-url", "http://localhost/blog", "URL du blog", "L'URL de base du blog (pour que les liens internes fonctionnent)"),
    ("theme", "default", "Theme", "Pour les couleurs, voir la doc.")
]


def config():
    config = {}
    print("\n")
    print("====  Bienvenue sur la configuration du rikitikiblog. =====\n")
    print("Il y a quelques valeurs à remplir, et puis c'est bon.\n")
    print("Sinon tu peux remplir directement dans le fichier var/config.json, si ça te botte.\n")
    for key, default, name, description in CONFIGURATION_SETUP:
        print(f"{name} ({description})")
        val = input(f"Si laissé vide, valeur par défaut = {default}:")
        if len(val.strip()) == 0:
            val = default
        config[key] = val
        print("\n------\n\n")

    with open('./var/config.json', 'w', encoding='utf-8') as fp:
        json.dump(config, fp)


def load_config():
    with open('./var/config.json', 'r', encoding='utf-8') as fp:
        return json.load(fp)



if __name__ == '__main__':
    config()