---
title: My post
date: 2023-12-31
draft: True
---

This is a test post.

You can publish it if you want, but I wouldn't recommend. You just have to remove the `draft: True` line hereabove, or set it to `False`, and it will be added to your blog the next time you use the `publish` script.

Otherwise, this can serve as a basic template.

## With headings

* and lists
* with items
    * and subitems

With *italic* and **bold**, with [links](https://gitlab.com/adfoucart/rikitikiblog) and even some $\LaTeX$ equations if you feel like it.

Most of the markdown functionalities should work fine. Its `pandoc` in the background, so whatever they can handle, this should handle it as well.