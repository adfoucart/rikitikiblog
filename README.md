# rikitikiblog

Un microblog radicalement minimaliste.

Je sais, ce n'est pas le premier, ni le meilleur, ni le dernier. Mais je l'ai codé pour faire mon blog -- https://adfoucart.be/blog -- et je le trouve pas trop mal, du coup ça plaira peut-être à d'autres ?

## Comment ça marche

* Lancer `config.py` pour configurer:
    * Nom & description du blog
    * Nom & email de l'auteur
    * URL du blog
    * Theme
* Pour écrire un post: écrire en format markdown dans le dossier `markdown` (logique)
* Lancer `publish.py` pour générer:
    * Une page HTML statique par post.
    * Une page d'index avec le dernier post complet + liens vers les 5 derniers posts.
    * Une page d'archives avec des liens vers tous les posts.
    * Une page "à propos" où tu mets ce que tu veux, vis ta vie.
    * Un fichier XML `feed.xml` avec un flux RSS.

Et c'est tout.

Tous les fichiers nécessaires au site seront écrits dans le dossier `www`. Si tu mets des images, il faut les rajouter dans le dossier `www/img` (et mettre les liens correctement dans le markdown: `![description](./img/lien-de-l-image.jpg)`)

Serveur web non inclus. Ce sont juste des fichiers HTML.

Pas de javascript, pas de traqueurs, pas de statistiques de vues, pas de commentaires, pas d'interactivité. C'est conçu pour crier dans le vide, le but de tout bon blog qui se respecte.